/**
 * Created by Juan on 22/4/16.
 */


function calculation() {
    $buyValue = $("#buy_selector").val();
    $sellValue = $("#sell_selector").val();

    //Set values on input
    $("#input_buy").val($buyValue)
    $("#input_sell").val($sellValue)
    
    if($buyValue != "empty" && $sellValue != "empty") {
        console.log("values are not empty");

        $.ajax({
            type: "GET",
            url:"/trading/rate/" + $sellValue + "/" + $buyValue,
            success: function(data) {
                console.log("Calculation done: " + data);
                $("#rate_currency").val(data);
                buyAmountCalculation();
            }
        });
    }else{
        console.log("values are empty");
    }
}

function buyAmountCalculation() {
    $sellAmount = $("#sell_fee").val();
    $rateCurrency = $("#rate_currency").val();

    if (!isNaN($sellAmount) && !isNaN($rateCurrency)) {
            $buyAmountResult = $sellAmount * $rateCurrency
        console.log("Buy amount: " + $buyAmountResult)

        $("#buy_fee").val($buyAmountResult)
    }
    else {
        $("#buy_fee").val("0")
    }
}