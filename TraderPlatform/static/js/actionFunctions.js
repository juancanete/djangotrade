/**
 * Created by Juan on 22/4/16.
 */

function deleteTransaction(id) {
    console.log("Deleting: " + id );
    $.ajax({
        type: "GET",
        url: "/trading/delete/" + id,
        success: function (data) {
            $('#transactions_table').load('/trading #transactions_table');
        }
    });
}