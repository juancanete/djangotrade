from trader.models import Transaction
from trader.models import Currency
from django.shortcuts import render
from django.http import HttpResponse, Http404
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from datetime import datetime

def transactionsList(request, message = ""):
    transactions = Transaction.objects.all()

    paginator = Paginator(transactions, 5)

    page = request.GET.get('page')
    try:
        transactions = paginator.page(page)
    except PageNotAnInteger:
        transactions = paginator.page(1)
    except EmptyPage:
        transactions = paginator.page(paginator.num_pages)

    return render(request, 'trading/trading.html', {'transaction_list': transactions, 'message': message})


#Create new Transaction
def newFormulary(request):
    currencies = Currency.objects.all()

    return render(request, 'trading/transaction.html',
                  {'titleModal':'New Trade','action': 'add', 'currencies':currencies,
         'actionButton': 'Create'}
                  )

def addTransaction(request):
    action = request.POST["submitted"]

    if (action == 'Create'):
        sellCurrency = Currency.objects.get(currency_code=request.POST["sell_currency"])
        sellAmount = request.POST["sell_amount"]
        buyCurrency = Currency.objects.get(currency_code=request.POST["buy_currency"])
        buyAmount = request.POST["buy_amount"]
        rate = request.POST["rate"]

        newTransaction = Transaction (
            sell_currency= sellCurrency,
            sell_amount= sellAmount,
            buy_currency=buyCurrency,
            buy_amount=buyAmount,
            rate=rate,
            date_booked=datetime.now()
        )
        newTransaction.save()

        return transactionsList(request, message="Transaction added")
    else:
        return transactionsList(request, message="Transaction canceled")

#Edit Transaction
def editFormulary(request,id):
    transaction = Transaction.objects.get(id=id)
    currencies = Currency.objects.all()

    return render(request, 'trading/transaction.html',
                  {'titleModal':'Editing Trade ' + id,
                   'currencies':currencies,
                   'actionButton':"Save",
                   'action':'update/' + id,
                   'sellCurrency':transaction.sell_currency,
                   'sellAmount':transaction.sell_amount,
                   'buyCurrency':transaction.buy_currency,
                   'buyAmount':transaction.buy_amount,
                   'rate':transaction.rate})

def updateTransaction(request,id):
    transaction = Transaction.objects.get(id=id)

    #Get string currencies
    stSellCurrency = request.POST["sell_currency"]
    stBuyCurrency = request.POST["buy_currency"]

    transaction.sell_currency = Currency.objects.get(currency_code=stSellCurrency)
    transaction.sell_amount = request.POST["sell_amount"]
    transaction.buy_currency = Currency.objects.get(currency_code=stBuyCurrency)
    transaction.buy_amount = request.POST["buy_amount"]
    transaction.rate = request.POST["rate"]

    transaction.save()

    return transactionsList(request,message="Transaction updated")

#Delete Transaction
def deleteFormulary(request, id):

    return render(request, 'trading/deleteTradeModal.html', {'titleModal':'Deleting Trade ' + id,
                            'idTrade':id})

def deleteTransaction(request,id):
    Transaction.objects.get(id=id).delete()
    return transactionsList(request,message="Transaction deleted")

#Get Rate
def rateCurrency(request, sellCurrency,buyCurrency):
    if request.is_ajax():
        rateSellCurrency = Currency.objects.get(currency_code=sellCurrency).rate
        rateBuyCurrency = Currency.objects.get(currency_code=buyCurrency).rate

        rate =  rateBuyCurrency/rateSellCurrency

        return HttpResponse("{0:.6f}".format(rate))
    else:
        raise Http404
