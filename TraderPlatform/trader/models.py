from django.db import models

def genKey():
    from base64 import b32encode
    from hashlib import sha1
    from random import random

    bad_pk = True
    while bad_pk:
        pk = 'TR' + b32encode(sha1(str(random())).digest()).upper()[:7]

        key = Transaction.objects.filter(id=pk.upper())

        if key.count() == 0:
            bad_pk = False

    return pk


class Currency (models.Model):
    currency_code = models.CharField(max_length=3, primary_key=True)
    name = models.CharField(max_length=20)
    description = models.CharField(max_length=2000)

    #Rate with regards to Dollar currency
    rate = models.DecimalField(max_digits=8, decimal_places=6)


class Transaction (models.Model):
    id = models.CharField(max_length=9, primary_key=True, default=genKey)
    sell_currency = models.ForeignKey(Currency, related_name='currency_for_sell') #models.CharField(max_length=3)
    sell_amount = models.DecimalField(max_digits=20, decimal_places=2)
    buy_currency = models.ForeignKey(Currency, related_name='currency_for_buy') #models.CharField(max_length=3)
    buy_amount = models.DecimalField(max_digits=20, decimal_places=2)
    rate = models.DecimalField(max_digits=7, decimal_places=5)
    date_booked = models.DateField()
