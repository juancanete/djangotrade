from trader.models import Currency
from trader.models import Transaction
from django.contrib import admin

admin.site.register(Currency)
admin.site.register(Transaction)
