from django.test import TestCase
from trader.models import Transaction
from trader.models import Currency
from datetime import datetime

class TransactionTest (TestCase):
    def setUp(self):
        Currency.objects.create(currency_code="USD",name="Dollar",rate=1, description="Dollar Currency")
        Currency.objects.create(currency_code="EUR", name="Euro", rate=1.12, description="Euro Currency")

        dollar = Currency.objects.get(currency_code="USD")
        euro = Currency.objects.get(currency_code="EUR")

        Transaction.objects.create(id="TRJB34B82",
                                   sell_currency=euro,
                                   sell_amount=500,
                                   buy_currency=dollar,
                                   buy_amount=500,
                                   rate=1,
                                   date_booked=datetime.now())


    def testRate(self):
        transaction = Transaction.objects.get(id="TRJB34B82")
        rate = transaction.rate
        buyAmount = transaction.buy_amount
        sellAmount = transaction.sell_amount

        self.assertEqual(sellAmount*rate,buyAmount)
