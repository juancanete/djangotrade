from django.conf.urls import patterns, include, url

from django.contrib import admin
import trader.views

admin.autodiscover()

urlpatterns = patterns('',
                       url(r'^admin/', include(admin.site.urls)),
                       url(r'^trading/$', 'trader.views.transactionsList'),
                       url(r'^trading/new', 'trader.views.newFormulary'),
                       url(r'^trading/add', 'trader.views.addTransaction'),
                       url(r'^trading/edit/(TR[A-Z0-9]{1,7})','trader.views.editFormulary'),
                       url(r'^trading/update/(TR[A-Z0-9]{1,7})','trader.views.updateTransaction'),
                       url(r'^trading/deleteModal/(TR[A-Z0-9]{1,7})', 'trader.views.deleteFormulary'),
                       url(r'^trading/delete/(TR[A-Z0-9]{1,7})','trader.views.deleteTransaction'),
                       url(r'^trading/rate/([A-Z]{3})/([A-Z]{3})', 'trader.views.rateCurrency'),
                       )
