insert into trader_currency(currency_code,name,description) values('USD', 'USA Dollar', 'Dollar from United States of America’, 1.0);

insert into trader_currency(currency_code,name,description) values('EUR', 'Euro', 'European Union Currency’, 0.890790);

insert into trader_currency(currency_code, name, description, rate) values ('GBP', 'Sterling', 'Sterling Currency', 0.69551);